package com.example.newrelic.core.services;

import com.example.newrelic.exceptions.BadRequestException;
import org.springframework.stereotype.Service;

@Service
public interface IGetService {
    String getSolicitation(String url, String token) throws BadRequestException;
}
