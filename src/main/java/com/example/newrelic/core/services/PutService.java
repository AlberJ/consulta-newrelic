package com.example.newrelic.core.services;

import com.example.newrelic.exceptions.BadRequestException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class PutService implements IPutService {

    @Override
    public String putSolicitation(String url, String token, String bodyContent) throws BadRequestException {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.set("X-Api-Key", token);
        headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);

        HttpEntity request = new HttpEntity(bodyContent, headers);

        try {
            ResponseEntity<String> response =
                    restTemplate.exchange(url, HttpMethod.PUT, request, String.class);

            return response.getBody();

        } catch (Exception e) {
            throw new BadRequestException();
        }
    }
}
