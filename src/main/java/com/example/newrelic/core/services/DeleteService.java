package com.example.newrelic.core.services;

import com.example.newrelic.exceptions.BadRequestException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class DeleteService implements IDeleteService {

    @Override
    public String deleteSolicitation(String url, String token) throws BadRequestException {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.set("X-Api-Key", token);

        HttpEntity request = new HttpEntity(headers);

        try {
            ResponseEntity<String> response =
                    restTemplate.exchange(url, HttpMethod.DELETE, request, String.class);

            return response.getBody();

        } catch (Exception e) {
            throw new BadRequestException();
        }
    }
}
