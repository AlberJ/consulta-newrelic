package com.example.newrelic.core.services;

import com.example.newrelic.exceptions.BadRequestException;
import org.springframework.stereotype.Service;

@Service
public interface IPutService {
    String putSolicitation(String url, String token, String bodyContent) throws BadRequestException;
}
