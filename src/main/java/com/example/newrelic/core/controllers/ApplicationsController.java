package com.example.newrelic.core.controllers;

import com.example.newrelic.core.services.IGetService;
import com.example.newrelic.core.services.IPutService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/applications")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ApplicationsController {

    private final static String urlNewRelic = "https://api.newrelic.com/v2/applications";
    private final String jsonFormatRequisitation = ".json";

    private String token = "NRRA-a72c4764961b7e357a5b81a06809fdb4c467ed78df";

    private final IGetService getService;

    private final IPutService putService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String list() throws Exception {

        String url = urlNewRelic.concat(jsonFormatRequisitation);

        return getService.getSolicitation(url, this.token);
    }

    @GetMapping(value = "/{applicationId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String show(@PathVariable String applicationId) throws Exception {

        String url = urlNewRelic.concat("/").concat(applicationId).concat(jsonFormatRequisitation);

        return getService.getSolicitation(url, this.token);
    }

    @GetMapping(value = "/metrics/{applicationId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String metricNames(@PathVariable String applicationId) throws Exception {

        String url = urlNewRelic
                .concat("/").concat(applicationId)
                .concat("/").concat("metrics")
                .concat(jsonFormatRequisitation);

        return getService.getSolicitation(url, this.token);
    }

    @PostMapping(value = "/data_metrics/{applicationId}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String metricData(@PathVariable String applicationId,
                             @RequestBody List<String> namesMetrics) throws Exception {

        String url = urlNewRelic
                .concat("/").concat(applicationId)
                .concat("/").concat("metrics")
                .concat("/").concat("data")
                .concat(jsonFormatRequisitation)
                .concat("?").concat("names[]=");

        for (String s : namesMetrics) {
            url = url.concat(s).concat("+");
        }

        return getService.getSolicitation(url, this.token);
    }

    @PutMapping(value = "/{applicationId}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String update(@PathVariable String applicationId,
                         @RequestBody String objectJson) throws Exception {

        String url = urlNewRelic
                .concat("/").concat(applicationId)
                .concat(jsonFormatRequisitation);


        return putService.putSolicitation(url, this.token, objectJson);
    }
}
