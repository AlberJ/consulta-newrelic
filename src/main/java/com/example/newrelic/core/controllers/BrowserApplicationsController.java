package com.example.newrelic.core.controllers;

import com.example.newrelic.core.services.IGetService;
import com.example.newrelic.core.services.IPostService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/browser_applications")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BrowserApplicationsController {

    private final static String urlNewRelic = "https://api.newrelic.com/v2/browser_applications";
    private final String jsonFormatRequisitation = ".json";

    private String token = "NRRA-a72c4764961b7e357a5b81a06809fdb4c467ed78df";

    private final IGetService getService;

    private final IPostService postService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String list() throws Exception {

        String url = urlNewRelic.concat(jsonFormatRequisitation);

        return getService.getSolicitation(url, this.token);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String create(@RequestBody String objectJson) throws Exception {

        String url = urlNewRelic.concat(jsonFormatRequisitation);

        return postService.postSolicitation(url, this.token, objectJson);
    }
}
