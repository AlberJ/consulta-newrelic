package com.example.newrelic.core.controllers;

import com.example.newrelic.core.services.IDeleteService;
import com.example.newrelic.core.services.IGetService;
import com.example.newrelic.core.services.IPostService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/application_deployments")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ApplicationDeploymentsController {

    private final static String urlNewRelic = "https://api.newrelic.com/v2/applications";
    private final String jsonFormatRequisitation = ".json";

    private String token = "NRRA-a72c4764961b7e357a5b81a06809fdb4c467ed78df";

    private final IGetService getService;

    private final IPostService postService;

    private final IDeleteService deleteService;

    @GetMapping(value = "/{applicationId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String list(@PathVariable String applicationId) throws Exception {

        String url = urlNewRelic
                .concat("/").concat(applicationId)
                .concat("/").concat("deployments")
                .concat(jsonFormatRequisitation);

        return getService.getSolicitation(url, this.token);
    }

    @PostMapping(value = "/{applicationId}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String create(@PathVariable String applicationId,
                         @RequestBody String objectJson) throws Exception {

        String url = urlNewRelic
                .concat("/").concat(applicationId)
                .concat("/").concat("deployments")
                .concat(jsonFormatRequisitation);

        return postService.postSolicitation(url, this.token, objectJson);
    }

    @DeleteMapping(value = "/{applicationId}/{deploymentId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String delete(@PathVariable String applicationId, @PathVariable String deploymentId) throws Exception {

        String url = urlNewRelic
                .concat("/").concat(applicationId)
                .concat("/").concat("deployments")
                .concat("/").concat(deploymentId)
                .concat(jsonFormatRequisitation);

        return deleteService.deleteSolicitation(url, this.token);
    }
}
